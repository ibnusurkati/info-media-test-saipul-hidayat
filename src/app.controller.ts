import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody } from '@nestjs/swagger';
import { User } from '@prisma/client';
import { AppService } from './app.service';
import { ChangePasswordnDTO } from './change-password.dto';
import { DataUser } from './data-user.decorator';
import { JwtAuthGuard } from './jwt/jwt.guard';
import { LoginDTO } from './login.dto';
import { PayloadDTO } from './payload.dto';
import { SingUpDTO } from './singup.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ApiBody({ type: PayloadDTO })
  @Post('signup')
  async signup(@Body() body: SingUpDTO) {
    return await this.appService.signup(body);
  }

  @ApiBody({ type: PayloadDTO })
  @Post('login')
  async login(@Body() body: LoginDTO) {
    return await this.appService.login(body);
  }

  @ApiBody({ type: PayloadDTO })
  @ApiBearerAuth()
  @Post('changePassword')
  @UseGuards(JwtAuthGuard)
  async changePassword(
    @Body() body: ChangePasswordnDTO,
    @DataUser() user: Partial<User>,
  ) {
    return await this.appService.changePassword(body, user.id);
  }
}
