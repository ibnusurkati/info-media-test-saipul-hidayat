import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

export class ChangePasswordnDTO {
  @ApiProperty()
  @Expose({ name: 'new_password' })
  @IsNotEmpty()
  @IsString()
  newPassword: string;

  @ApiProperty()
  @Expose({ name: 'old_password' })
  @IsNotEmpty()
  @IsString()
  oldPassword: string;
}
