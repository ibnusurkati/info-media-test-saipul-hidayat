import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { PrismaService } from 'src/prisma.service';
import { JwtStrategy } from './jwt.strategy';

const JwtModuleRegister = JwtModule.register({
  secret: 'bismillah',
  signOptions: { expiresIn: '1h' },
});

@Module({
  imports: [PassportModule, JwtModuleRegister],
  providers: [JwtStrategy, PrismaService],
  exports: [JwtModuleRegister, JwtStrategy],
})
export class JwtAuthModule {}
