import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly prisma: PrismaService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'bismillah',
    });
  }

  async validate(payload: { sub: string }) {
    try {
      const user = await this.prisma.user.findFirstOrThrow({
        where: { id: parseInt(payload.sub) },
        select: {
          id: true,
          email: true,
          username: true,
        },
      });

      return user;
    } catch (error) {
      throw new UnauthorizedException();
    }
  }
}
