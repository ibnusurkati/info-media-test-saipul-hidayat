import {
    Injectable,
    OnModuleDestroy,
    OnModuleInit
} from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService
  extends PrismaClient
  implements OnModuleInit, OnModuleDestroy
{
  constructor() {
    super({
      log: ['query', 'info', 'warn', 'error'],
    });
  }
  async onModuleInit() {
    let retries = 5;
    while (retries > 0) {
      try {
        await this.$connect();

        break;
      } catch (err) {
        retries -= 1;

        await new Promise((res) => setTimeout(res, 3_000));
      }
    }
  }

  async onModuleDestroy() {
    await this.$disconnect();
  }
}
