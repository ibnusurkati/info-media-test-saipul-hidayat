import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { ChangePasswordnDTO } from './change-password.dto';
import { LoginDTO } from './login.dto';
import { PrismaService } from './prisma.service';
import { SingUpDTO } from './singup.dto';

@Injectable()
export class AppService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly prisma: PrismaService,
  ) {}

  async signup(body: SingUpDTO) {
    const user = await this.prisma.user.create({
      data: {
        email: body.email,
        username: body.username,
        password: {
          create: {
            password: bcrypt.hashSync(body.password, bcrypt.genSaltSync(10)),
          },
        },
      },
    });

    const token = this.jwtService.sign({ sub: user.id });
    return { token };
  }

  async login(body: LoginDTO) {
    try {
      const user = await this.prisma.user.findFirstOrThrow({
        where: { username: body.username },
        include: {
          password: {
            select: { password: true },
            take: 1,
            orderBy: { id: 'desc' },
          },
        },
      });

      if (user.isBlock) throw new Error('BLOCK');
      if (user.passwordInvalidCount >= 4) {
        await this.prisma.user.update({
          data: { isBlock: true },
          where: { id: user.id },
        });
        throw new Error('INVALID');
      }

      const userPassword = user.password[0].password;
      const isValid = bcrypt.compareSync(body.password, userPassword);
      if (!isValid) {
        await this.prisma.user.update({
          data: { passwordInvalidCount: user.passwordInvalidCount + 1 },
          where: { id: user.id },
        });
        throw new Error('INVALID');
      }

      const token = this.jwtService.sign({ sub: user.id });

      return { token };
    } catch (error) {
      if (error.message === 'BLOCK')
        throw new BadRequestException('Akun anda telah di blokir');
      if (error.message === 'INVALID')
        throw new BadRequestException('username atau password salah');
    }
  }

  async changePassword(body: ChangePasswordnDTO, userId: number) {
    try {
      console.log(body);
      
      if (body.newPassword === body.oldPassword)
        throw new Error('SAME_PASSWORD');

      const user = await this.prisma.user.findFirstOrThrow({
        where: { id: userId },
        include: {
          password: {
            select: { password: true },
            take: 5,
            orderBy: { id: 'desc' },
          },
        },
      });

      for (const userPassword of user.password) {
        if (bcrypt.compareSync(body.newPassword, userPassword.password)) {
          throw new Error('SAME_WITH_OLD_PASSWORD');
        }
      }

      await this.prisma.user.update({
        data: {
          password: {
            create: {
              password: bcrypt.hashSync(
                body.newPassword,
                bcrypt.genSaltSync(10),
              ),
            },
          },
        },
        where: { id: user.id },
      });

      return {
        success: true,
        message: 'Password berhasil di ubah',
      };
    } catch (error) {
      if (error.message === 'SAME_PASSWORD')
        throw new BadRequestException(
          'Password baru dan password lama tidak boleh sama',
        );
      if (error.message === 'SAME_WITH_OLD_PASSWORD')
        throw new BadRequestException(
          'Password baru tidak boleh sama dengan password yang sebelumnya pernah di pakai',
        );
    }
  }
}
