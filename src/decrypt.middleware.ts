import {
  Injectable,
  InternalServerErrorException,
  NestMiddleware,
} from '@nestjs/common';
import * as CryptoJS from 'crypto-js';
import { Request, Response } from 'express';

@Injectable()
export class DecryptMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: Function) {
    if (req.body) {
      try {
        const key = '455F29zMZ0vfllGZToiqN9SqxtJCQtUo';
        const iv = CryptoJS.enc.Utf8.parse('PDBLyv9HSMNWgrIp');

        const decrypted = CryptoJS.AES.decrypt(req.body.payload, key, {
          iv,
        }).toString(CryptoJS.enc.Utf8);
        req.body = JSON.parse(decrypted);
      } catch (error) {
        throw new InternalServerErrorException(error.message);
      }
    }
    next();
  }
}
