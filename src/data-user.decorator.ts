import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from '@prisma/client';

export interface RequestWithDataInterface extends Request {
  user?: Partial<User>;
}

export const DataUser = createParamDecorator(
  (_: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest<RequestWithDataInterface>();
    return request.user;
  },
);
