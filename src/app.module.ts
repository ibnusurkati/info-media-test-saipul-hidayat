import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DecryptMiddleware } from './decrypt.middleware';
import { JwtAuthModule } from './jwt/jwt.module';
import { PrismaService } from './prisma.service';

@Module({
  imports: [JwtAuthModule],
  controllers: [AppController],
  providers: [AppService, PrismaService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(DecryptMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
